import React from "react";
import logo from './logo.svg';
import './App.css';

function PrintDetails(details) {
  console.log(details);
  var prettyDetails = JSON.stringify(details, null, 10);

  return <div className="pretty">{prettyDetails}</div>
}

function App() {
  const [data, setData] = React.useState(null);
  const [queuePrintout, setqueuePrintout] = React.useState(null);
  const [selectedItem, setSelectedItem] = React.useState(null);
  const [itemDetails, setItemDetails] = React.useState(null);
  const [buttonWatcher, setbuttonWatcher] = React.useState(0);

  React.useEffect(() => {
    fetch("/queues")
      .then((res) => res.json())
      .then((data) => {
        setData(data.message);
      });
  }, []);

  React.useEffect(() => {
    if (data) {
      let sss = data.map(element =>
        <li key={element}>
          <button onClick={(event) => {
            event.preventDefault();
            setSelectedItem(element);
            setbuttonWatcher(buttonWatcher + 1);
          }}>
            {element.slice(element.lastIndexOf("/") + 1)}
          </button>
        </li>
      );
      setqueuePrintout(sss);
    }
  }, [data]);

  React.useEffect(() => {
    fetch("/queue?url=" + encodeURI(selectedItem))
      .then((res) => res.json())
      .then((data) => {
        setItemDetails(data.message);
      });
  }, [selectedItem, buttonWatcher]);

  return (
    <div className="App">
      <div className="LeftSide">
        {!queuePrintout ? "Loading..." : queuePrintout}
      </div>
      <div className="RightSide">
        {!itemDetails ? "Select item from list to show details" : PrintDetails(itemDetails)}
      </div>
    </div>
  );
}

export default App;
