// server/index.js

const express = require("express");

const PORT = process.env.PORT || 3001;

const app = express();

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});

app.get("/api", (req, res) => {
    res.json({ message: "Hello from server!" });
});

var AWS = require('aws-sdk');
var credentials = new AWS.SharedIniFileCredentials({ profile: 'default' });
AWS.config.credentials = credentials;
AWS.config.update({ region: 'eu-central-1' });

// Create an SQS service object
var sqs = new AWS.SQS();

app.get("/queues", (req, res) => {
    var params = {};
    sqs.listQueues(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
            res.json({ message: data.QueueUrls });
        }
    });
});

app.get("/queue", (req, res) => {
    var queueUri = decodeURI(req.query.url);

    var params = {
        QueueUrl: queueUri,
        AttributeNames: ['All']
    };

    sqs.getQueueAttributes(params, function (err, data) {
        if (err) {
            console.log(err, err.stack); // an error occurred
        }
        else {
            res.json({ message: data });
        }
    });
});

// All other GET requests not handled before will return our React app
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
});